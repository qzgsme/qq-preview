#!/bin/sh
if [ -f /data/config.py ]
then
    cp /data/config.py /usr/src/app
fi

exec "$@"
