#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import json
import requests
import argparse
from config import email_config
from time import sleep


def parse_jsonp(jsonp):
    # parse qq jsonp
    json_str = jsonp[jsonp.index("{"):jsonp.rindex(";")]
    try:
        json_response = json.loads(json_str)
        return json_response
    except json.decoder.JSONDecodeError as e:
        logging.error("Cannot parse json, error: {}".format(e))
        return None


def get_preview():
    qq_url = "http://s.video.qq.com/load_poster_list_info"

    qq_params = {
        'otype': 'json',
        'plat': 2,
        'id': 'ttqxo9s3cblgofl',
        'req_type': 12
    }

    qq_headers = {
        'User-Agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
    }

    r = requests.get(qq_url, headers=qq_headers, params=qq_params)

    if r.status_code == 200:
        preview_response = r.text
        return parse_jsonp(preview_response)

    else:
        logging.error("Cannot get {}".format(qq_url))
        return None


def check_preview(preview_id):
    # 获取返回的 Json
    preview_response = get_preview()
    qq_preview_list = None

    if preview_response is None:
        return None

    # 解析预告片
    modList = preview_response['modList']

    for el in modList:
        if el['modTitle'] == '预告片':
            qq_preview_list = el['posterList']
            break

    if qq_preview_list is None:
        logging.error("Parse 预告片 error!")
        return None

    # 寻找预告片
    for el in qq_preview_list:
        if el['title'] == "预告片_{}".format(preview_id):
            logging.info("Find {}, url: {}".format(el['title'], el['url']))
            return el['url']

    logging.error("Cannot find 预告片 {}!".format(preview_id))
    return None


def send_mailgun_email(send_msg):
    send_url = email_config['api_url']
    send_auth = ("api", email_config['api_key'])
    send_data = {
        "from": email_config['from'],
        "to": email_config['to'],
        "subject": send_msg['title'],
        "text": send_msg['body'],
    }
    logging.info("Send email to {}".format(email_config['to']))
    r = requests.post(send_url, auth=send_auth, data=send_data)

    if r.status_code == 200:
        res_msg = r.json().get('message', None)
        logging.info("Send email success with: {}".format(res_msg))
    else:
        logging.error("Cannot send email to {}".format(email_config("to")))


def run_daemon(preview_id):
    # try every 30s, total duration 1 hour
    sleep_interval = 30
    max_try_times = 5760
    try_times = 0
    preview_res = None

    while True:
        preview_res = check_preview(preview_id)
        if preview_res:
            break

        sleep(sleep_interval)
        try_times += 1

        if try_times > max_try_times:
            break

    return preview_res


def run(preview_id, daemon_mode):
    if daemon_mode:
        preview_res = run_daemon(preview_id)
    else:
        preview_res = check_preview(preview_id)

    if preview_res is None:
        return

    email_msg = {
        'title': '找到预告片 {}'.format(preview_id),
        'body': '找到预告片：{}\n下载地址：{}'.format(preview_id, preview_res)
    }

    send_mailgun_email(email_msg)


def init_arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--preview", help="qq preview id")
    parser.add_argument("-d", "--daemon", help="daemon mode", action="store_true")
    args = parser.parse_args()

    if args.preview:
        run(args.preview, args.daemon)
    else:
        parser.print_help()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)
    init_arg_parse()
