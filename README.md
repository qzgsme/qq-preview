# v.qq.com preview 

### RUN

Prepare your config file in YOUR_DATA_FOLDER then run:

```bash
docker run -t --rm -v YOUR_DATA_FOLDER:/data qzgsme/qq-preview
```

### Config

```python
email_config = {
    'from': 'xxx',
    'to': 'xxx',
    'username': 'xxx',
    'password': 'xxx',
    'smtp_server': 'xxx',
    'smtp_port': 465,
}
```
